/*
 *varios ejercicios usando metodos de String
 */
package Modelo;



/**
 *
 * @author mireya Bernal
 */
public class EjercicioStringLetras {
    
    
    String palabra;
    
    public int contarletra (String cadena, char l) {
        
        int c = 0;
        for (int i = 0; i < cadena.length(); i++) {
            if (cadena.charAt(i) == l ) {
                
                c=c+1;
             }
        }
       
        return c;
    }
    
    
    
    public String CompararIgual(String cadena1, String cadena2) {
        
        
            if (cadena1.equalsIgnoreCase(cadena2)){
               return "las palabras son iguales";
            }else{
        
         return "Las palabras son diferentes";
            }
          
    }
    
    
    
     public String recortar(String cadena) {
        
        String cadena1;
           cadena1=cadena.substring(1,4);
        
         return cadena1;
            
          
    }
     
     public boolean validarNombre(String cadena){
         boolean e=true;
          for (int i = 0; i < cadena.length(); i++) {
               if(cadena.charAt(i)!=' ' ){
                  if( cadena.charAt(i)<65 || cadena.charAt(i)>=91 && cadena.charAt(i)<=96 || cadena.charAt(i)>122  ){
                   e=false;
               }
          }
         }
          return e;
     }
    
     
      public int tamano(String cadena) {
        
       int c=0;
        for(int i=0; i<cadena.length();i++){
            if(cadena.charAt(i)!=' '){
             c++;
        }
        }
         
        
         return c;
            
          
    }
    
      public boolean palindrome(String cadena) {
          boolean r=true;
      
        for (int i = 0, j = cadena.length() - 1; i < cadena.length() / 2; i++, j--){
    
               if (cadena.charAt(i) != cadena.charAt(j)){
                     r=false;
            }
        }
        return r;
      }
    
  
    
    }
    
     
    

