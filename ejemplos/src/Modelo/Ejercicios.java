package Modelo;
 import Vista.Entrada_Salida;
 

public class Ejercicios {
	Entrada_Salida INOUT =new Entrada_Salida();

	public void Cuantas_Letras(){ 
		String Palabra;
		int Conteo;
		
		Palabra=INOUT.Text_int("Escriba la palabra");	
		Conteo=Palabra.replace(" ","").length();		
		INOUT.Print_out(Conteo + " Letras");
					
	}
	
	public void Letras_Abc(){
		String Palabra,Faltantes="";
		String sep = System.getProperty("line.separator");
		int CodeASCCI=0;
		Boolean FlagOK=false;
		
		Palabra=INOUT.Text_int("Escriba la Frase");	
		
		for(int i=65;i<91;i++){
			FlagOK=false;
			for(int x=0;x<Palabra.length();x++){
		CodeASCCI=Palabra.toUpperCase().codePointAt(x);

				if (CodeASCCI==i){
					FlagOK=true;
					
					}
				
				}
			
				if (FlagOK==false){
					Faltantes=Faltantes+sep+(i-64)+" -> "+(char)i;
				}	
	}
		INOUT.Print_out("Las letras faltantes son "+sep +Faltantes);	
					
	}
	
	
	public void Palabras_Frase(){
		String Palabra;
		int Conteo=1;
		
		Palabra=INOUT.Text_int("Escriba la Frase");	
		Palabra=Palabra.trim();	
		for (int x=0;x<Palabra.length();x++){
		if (Palabra.charAt(x)==(char)32){
			Conteo=Conteo+1;
		}
		}
		INOUT.Print_out("Tiene "+Conteo+" Frases");
		       
	}
	
	public void Validar_Nombre(){
		String Palabra;
		
		
		Palabra=INOUT.Text_int("Escriba el Nombre");	
	    if (Palabra.equalsIgnoreCase("Perencejo del Rio")){
	    INOUT.Print_out("Si se llama");}
	    else{INOUT.Print_out("No se llama");}
	    }
	
	public void N_Vocales(){

		String Palabra;
		int Conteo=0;
		
		Palabra=INOUT.Text_int("Escriba la palabra");	
	    for (int x=0;x<Palabra.length();x++){
	    	if (Palabra.toLowerCase().codePointAt(x)==97||
	    		Palabra.toLowerCase().codePointAt(x)==101||
	    		Palabra.toLowerCase().codePointAt(x)==105||
	    		Palabra.toLowerCase().codePointAt(x)==111||
	    		Palabra.toLowerCase().codePointAt(x)==117){
	    		Conteo=Conteo+1;
	    	}
	    	
	    }
	    INOUT.Print_out("Tiene " + Conteo +" Vocales");
	    }
	}
		
	
	


