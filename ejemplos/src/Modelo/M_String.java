package Modelo;
import java.util.Scanner;
import java.util.Random;
public class M_String {
	 Scanner Scan = new Scanner(System.in);
	 Random Random=new Random();
	public void Ej_String(){
		String Palabra1,Palabra2,Palabra3,Palabra4,Palabra5,Palabra6,Palabra7,Palabra8;
		String[] Palabras;
		Palabras=new String[4];
		char Ch1,Ch2;
		CharSequence Cs1;
		int Largo;
		Ch1=65;
		Ch2=66;
		Cs1="Hola";
		Palabra1="Hola Henry";
		Palabra2="Hola Henry";
		Palabra3="Hola Henrys";
		Palabra4="Hola HHH";
		Palabra5=Palabra2.toUpperCase();
		Palabra6="";
		Palabra7="   Hola  mi    nombre   es   Henrys  ";
		Palabra8="Henry-Andres-Benitez-Arciniegas";
		Largo=Palabra1.length();
		Palabras[0]="Henry";
		Palabras[1]="Andres";
		Palabras[2]="Benitez";
		Palabras[3]="Arciniegas";
		System.out.println("1->"+Palabra1.charAt(3));
		System.out.println("2->"+Palabra1.codePointAt(0));
		System.out.println("3->"+Palabra1.codePointBefore(2));
		System.out.println("4->"+Palabra1.codePointCount(0,Largo));
		System.out.println("5->"+Palabra1.compareTo(Palabra5));
		System.out.println("6->"+Palabra1.compareToIgnoreCase(Palabra5));
		System.out.println("7->"+Palabra1.concat(Palabra4));
		System.out.println("8->"+Palabra1.contains("4"));
		System.out.println("9->"+Palabra1.contentEquals(Cs1));	// No se !!!!!!	
		//System.out.println("10->"+Palabra1.copyValueOf([2]));	// No se !!!!!!	
		System.out.println("11->"+Palabra1.endsWith("Henry"));
		System.out.println("12->"+Palabra1.equals(Palabra5)); //Duda frente al 5
		System.out.println("13->"+Palabra1.equalsIgnoreCase(Palabra5)); //Duda frente al 6
		System.out.println("14->"+Palabra4.indexOf(72)); //duda
		System.out.println("15->"+Palabra1.intern()); //duda
		System.out.println("16->"+Palabra6.isEmpty()); 
		System.out.println("17->"+Palabra6.lastIndexOf(65)); 
		System.out.println("18->"+Palabra1.length()); 
		String[] Palabras2=Palabra8.split("-");
		System.out.println("19->"+Palabras2[0]+ Palabras2[1]+ Palabras2[2] + Palabras2[3]); // Solo con Vector
		System.out.println("20->"+Palabra1.startsWith("Ho"));
		System.out.println("21->"+Palabra1.substring(2,4));
		System.out.println("22->"+Palabra1.toLowerCase());
		System.out.println("23->"+Palabra1.toUpperCase());
		System.out.println("24->"+Palabra7.trim());
		int range=3-0+1; 
		System.out.println("25->"+Palabras[(Random.nextInt(range)+0)]);
		
		
	}
	
	
}
