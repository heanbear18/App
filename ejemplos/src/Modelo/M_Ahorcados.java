package Modelo;


import java.util.Random;
import Vista.Entrada_Salida;
public class M_Ahorcados {
	public String[] Palabras;
	
	Entrada_Salida inx= new Entrada_Salida();
	Random RN=new Random();
	
		public void Vector_palabras(){
			Palabras =new String[10];			
			Palabras[0]="MUTE";
			Palabras[1]="SECO";
			Palabras[2]="ZIKA";
			Palabras[3]="SOLO";
			Palabras[4]="RATA";
			Palabras[5]="MICO";
			Palabras[6]="PURO";
			Palabras[7]="GATO";
			Palabras[8]="HILO";
			Palabras[9]="ROCA";
			
		}
		
		
		
		
		public void Ahorcado(){
			String sep = System.getProperty("line.separator");
			String palabra="";
			
			
		int Rango=10-0+1;
		int Flag1=0,Flag2=0;
		
		while(Flag1==0){
			palabra=inx.Text_int("Ingrese una palabra de cuatro caracteres ");
			
			
			boolean No_Numeros=true;
					No_Numeros=Solo_Numeros(palabra);
			
			if (palabra.length()!=4 || No_Numeros==false ){
			inx.Print_out("Digite una palabra de cuatro caracteres que no sean numeros ");
			palabra="";
			}else{Flag1=1;}
		}
		 Flag1=0;
		 
		 String[] Dibujo;
		 Dibujo =new String[4];
		 Dibujo[0]=Pic_Ah0().replace(".", " ");
		 Dibujo[1]=Pic_Ah1().replace(".", " ");
		 Dibujo[2]=Pic_Ah2().replace(".", " ");	
		 Dibujo[3]=Pic_Ah3().replace(".", " ");	
		
				String palabra_random; 
				palabra_random =Palabras[RN.nextInt(Rango)+0];
			
				int Acierto,Fallo,Final,Game_Over;		
				
				char Chp,Chr;
				
				Acierto=0;
				Fallo=0;
				Final=0;
				Game_Over=0;
				
				while(Flag2==0){
					Acierto=0;
					Fallo=0;
				
				for (int i=0;i<4;i++){
					Chr=palabra.toUpperCase().charAt(i);
					Chp=palabra_random.charAt(i);
					
					if ( Chr==Chp ) {
						Acierto=Acierto+1;
					
					}
						else {
						Fallo=Fallo+1;	
						}
							
					}
				if (Fallo>0){Final=Final+1;}
			
				if (Final==3){Flag2=1;}else{Flag1=0;}
				if (Acierto==4){Flag2=1; Game_Over=1; Flag1=1;}
			
			
				while(Flag1==0){
					palabra=inx.Text_int(Dibujo[Final]+sep+"Aciertos = " + Acierto+sep +"Fallos = " +Fallo +sep
							+ "Intentos Restantes = "+(3-Final));
					
					
					if (palabra.length()!=4){
					inx.Print_out("Digite una palabra de cuatro caracteres ");
					palabra="";
					}else{Flag1=1;}
				}	
				
				
			}
				if (Game_Over==1){inx.Print_out(Dibujo[Final]+sep+"Ganaste");}
				else{inx.Print_out(Dibujo[Final]+sep+ "Perdiste");}
		}
		
		
		
		
		
		
		public  static String Pic_Ah3(){
			String sep = System.getProperty("line.separator");
			char MD=(char)92;
			char MI=(char)47;
			char Cabeza=(char)79;
	
			String Pic;
			  Pic="........._______...."
		   + sep +"........./...............||..."
		   + sep +"........"+Cabeza+"....... ......||..."			  
		   + sep +"......."+MI+" || "+MD+"............||..."
		   + sep +".........||..............||..."	
		   + sep +".......''..''............||..."	
		   + sep +".........................||..."	
		   + sep +".........................||..."	
		   + sep +".=============..."	
		   ;
			  return Pic;	  
		
			
		}
		
		public  static String Pic_Ah1(){
			String sep = System.getProperty("line.separator");
			
			char Cabeza=(char)79;
	
			String Pic;
			  Pic="........._______...."
		   + sep +"........./...............||..."
		   + sep +"........"+Cabeza+"....... ......||..."			  
		   + sep +".........................||..."	
		   + sep +".........................||..."	
		   + sep +".........................||..."	
		   + sep +".........................||..."	
		   + sep +".........................||..."	
		   + sep +".=============..."	
		   ;
		return Pic;	  

			
			
		}
		
		public  static String Pic_Ah2(){
			String sep = System.getProperty("line.separator");
			char MD=(char)92;
			char MI=(char)47;
			char Cabeza=(char)79;
	
			String Pic;
			  Pic="........._______...."
		   + sep +"........./...............||..."
		   + sep +"........"+Cabeza+"....... ......||..."			  
		   + sep +"......."+MI+" || "+MD+"............||..."
		   + sep +".........................||..."
		   + sep +".........................||..."
		   + sep +".........................||..."	
		   + sep +".........................||..."	
		   + sep +".=============..."	
		   ;

			  return Pic;	  		
			
		}
		public  static String Pic_Ah0(){
			String sep = System.getProperty("line.separator");
			
	
			String Pic;
			  Pic=" ||----------------------------||"
		   + sep +" ||---MAXIMA--------------||"
		   + sep +" ||-------PUNTUACION---||"
		   + sep +" ||----------------------------||"	
		   + sep +" ||----MUY BIEN-----------||"	
		   + sep +" ||----------------------------||"+sep	
		   ;

			  return Pic;	  		
			
		}
		
		public boolean Solo_Numeros(String Word){
			boolean Z=true;
			for(int x=0;x<Word.length()-1;x++){
				int V=Word.toUpperCase().codePointAt(x);
				
				if(V<65 || V>90){
				Z=false;	
				
			}
		}
			return Z;
		}
}	