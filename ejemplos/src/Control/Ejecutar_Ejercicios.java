package Control;
import Modelo.Ejercicios;
import Vista.Entrada_Salida;
public class Ejecutar_Ejercicios {
	public static void main (String[] ar){
		Ejercicios EJ = new Ejercicios();
		Entrada_Salida IN=new Entrada_Salida();
		String sep = System.getProperty("line.separator");
		int Sel=0;
		String SelS;
		Boolean Flag=false;
		try{SelS=IN.Text_int("-------------------------------------------Menu------------------------------------------"+sep+
						  "1- Determinar cuantas letras tiene un palabra"+sep+
						  "2- Determinar que letras del abecedario no estan en la frase"+sep+
						  "3- Determinar cuantas palabras tiene una frase"+sep+
						  "4- Determinar si una persona se llama Perencejo del Rio"+sep+
						  "5- Determinar cuantas vocales tiene una palabra");
		Sel=Integer.parseInt(SelS);}
		catch(Exception Es){ IN.Print_out("Seleccion invalida") ;Flag=true;  }
		switch (Sel){
		case 1: EJ.Cuantas_Letras();
				break;
		case 2: EJ.Letras_Abc();
			break;
		case 3: EJ.Palabras_Frase();
			break;
		case 4: EJ.Validar_Nombre();
			break;
		case 5: EJ.N_Vocales();
			break;
		default: 
			if (Flag==false){IN.Print_out("Seleccion no es valida");} 	
		}
		
		}

}
